import codecs
dic={}
with codecs.open("data/dict/lang_char.txt", "r", "utf-8") as f:
    for line in f.readlines():
        dic[line.strip().split()[1]] = line.strip().split()[0]
    while True:
        stdin = input().strip().split(",")
        for i in stdin:
            print(dic[i.replace(" ", "")])
        
