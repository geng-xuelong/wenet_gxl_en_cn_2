dir=exp/final_work_stage5
average_num=5
echo "do model average and final checkpoint is $decode_checkpoint"
python wenet/bin/average_model.py \
    --dst_model $dir/avg.pt \
    --src_path $dir  \
    --mode hybrid  \
    --num ${average_num} \
    --val_best
